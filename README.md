# Laravel Docker Image

This is a **modified** fork of the https://github.com/lorisleiva/laravel-docker generic docker image for Laravel applications that is used for Laravel projects in GitLab CI pipelines. We maintain a fork for customization and software supply chain security review reasons.

> **Disclaimer:** This is not an official package maintained by any company.
>
> Please use at your own risk.
>
> Please contribute to the [upstream repository](https://github.com/lorisleiva/laravel-docker). We do not maintain a roadmap of community feature requests, however we invite you to contribute and we will gladly review your merge requests.

## How It Works

### Use Image in `gitlab-ci.yml`

See the [GitLab Documentation](https://docs.gitlab.com/ee/ci/yaml/#image) for an overview of using `image:` in a `.gitlab-ci.yml` file.

To use the Provisionesta fork image that is hosted on the GitLab Container Registry. We specify the tag of the PHP version that you would like to use.

```
image: registry.gitlab.com/provisionesta/laravel-docker:8.2
```

The normal image includes all features used for build and testing CI pipelines. The `-nginx` variant removes the build tools and is designed for GCP Cloud Run deployments with a smaller image file size.

```
image: registry.gitlab.com/provisionesta/laravel-docker:8.2-nginx
```

To use the original upstream image, use the Docker Hub image. This can be helpful for validating the source of any errors.

```
image: lorisleiva/laravel-docker:8.2
```

### Image Versions

| Tags       | PHP version | Features                    | CI Image URL |
|------------|-------------|-----------------------------|--------------|
| 8.0        | 8.0         | ✅ Everything.              | `registry.gitlab.com/provisionesta/laravel-docker:8.0` |
| 8.1        | 8.1         | ✅ Everything.              | `registry.gitlab.com/provisionesta/laravel-docker:8.1` |
| 8.1-nginx  | 8.1         | Missing g++,gcc,nodejs,yarn | `registry.gitlab.com/provisionesta/laravel-docker:8.1-nginx` |
| 8.2        | 8.2         | ✅ Everything.              | `registry.gitlab.com/provisionesta/laravel-docker:8.2` |
| 8.2-nginx  | 8.2         | Missing g++,gcc,nodejs,yarn | `registry.gitlab.com/provisionesta/laravel-docker:8.2-nginx` |

### Example GitLab CI Pipeline Output

```bash
Preparing the "docker" executor
Using Docker executor with image registry.gitlab.com/provisionesta/laravel-docker:8.2 ...
Pulling docker image registry.gitlab.com/provisionesta/laravel-docker:8.2 ...
Using docker image sha256:93341554b4966c4bac9ffe99c35d1b46feeb81c939da8600282d2098549b4529 for registry.gitlab.com/provisionesta/laravel-docker:8.2 with digest registry.gitlab.com/provisionesta/laravel-docker@sha256:6238cfe669dd2d81bcb0c9354115f75599a3efa147857ad131b1c6fe22e2bebe ...
Preparing environment
```

### Fork Pull Requests

The fork maintainers review new versions of PHP on an as-needed basis and add support for each version after it is available in the upstream repository.

### Image Compiling

The GitLab CI [scheduled pipeline](https://gitlab.com/provisionesta/laravel-docker/-/pipeline_schedules) `Weekly Image Updates` runs weekly on Monday at 18:00 UTC (10:00am PT) to get the latest versions of all packages in the `Dockerfile` so any build failures occur during normal working hours for maintainers.

The CI pipeline can be run at any time by maintainers if a version vulnerability is discovered and the latest version has a patch.

## Maintainers

### Original Repository

[https://github.com/lorisleiva/laravel-docker](https://github.com/lorisleiva/laravel-docker)

Author Credit: [Loris Leiva](https://github.com/lorisleiva)

### Fork Maintainers

| Name | GitLab Handle |
|------|---------------|
| [Dillon Wheeler](https://about.gitlab.com/company/team/#dillonwheeler) | [@dillonwheeler](https://gitlab.com/dillonwheeler) |
| [Jeff Martin](https://about.gitlab.com/company/team/#jeffersonmartin) | [@jeffersonmartin](https://gitlab.com/jeffersonmartin) |

## Tutorials

* [Run test suite and check codestyle](http://lorisleiva.com/using-gitlabs-pipeline-with-laravel/)
* [Build, test and deploy your Laravel application](http://lorisleiva.com/laravel-deployment-using-gitlab-pipelines/)
